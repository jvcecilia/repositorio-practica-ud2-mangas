package gui;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * Created by DAM on 18/12/2018.
 */
public class Vista extends JFrame{

    //TabbedPanel
    private JTabbedPane tabbedPane1;

    //Panel Tienda
    private JPanel panel1;
    private JTextField textField1;
    private JTextField textField2;
    private JTextField textField3;
    private JTextField textField4;
    private JTextField textField5;
    private JTable table1;
    private JButton modificarButton;
    private JButton altaButton;
    private JButton eliminarButton;
    private DefaultTableModel dtm;

    //JMenuBar
    JMenuItem datosConexion;
    JMenuItem mirarDatosConexion;


    //Dialog conexion
    private JDialog dialogDatosConexion;
    JTextField tfUser;
    JTextField tfPass;
    JButton btnGo;

    public Vista() {
        setJMenuBar(menuBarPersonalizada());
        iniciarTabla();
        setTitle("Acceso a base de datos");
        setContentPane(panel1);
        setExtendedState(JFrame.MAXIMIZED_BOTH);
        //setUndecorated(true); Sin bordes
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        pack();
        setVisible(true);
    }

    /**
     * Setea el DefaultTableModel de la tabla 1. Le digo a ese DTM el nombre de los identificadores de cada columna.
     */
    private void iniciarTabla(){
        this.table1.getTableHeader().setReorderingAllowed(false);
        dtm = new DefaultTableModel();
        table1.setModel(dtm);

        Object[] cabeceraTablaTienda = {"ID","Nombre","Direccion","Descripcion","Encargado","nº Trabajadores"};

        dtm.setColumnIdentifiers(cabeceraTablaTienda);

    }

    /**
     * JMenuBar del programa
     * @return JMenuBar que tendra 2 items relacionados con los datos de conexion.
     */
    private JMenuBar menuBarPersonalizada(){
        JMenuBar barra = new JMenuBar();
        JMenu menu = new JMenu("Administrador");
        mirarDatosConexion = new JMenuItem("Datos de conexion");
        datosConexion = new JMenuItem("(Administrador) Modifica los datos de conexion");

        datosConexion.setActionCommand("escribirDatosConexion");
        mirarDatosConexion.setActionCommand("leerDatosConexion");

        menu.add(mirarDatosConexion);
        menu.add(datosConexion);

        barra.add(menu);
        return barra;
    }

    /**
     * Se muestra un JDialog para que el administrador rellene los datos de conexion del programa.
     * @return El JDialog para rellenar los datos de conexion
     */
    public JDialog dialogDatosConexion(){
        dialogDatosConexion = new JDialog();


        // Panel border layout para meter en el center los campos para rellenar informacion y en el sur el boton para transportar la info al fichero
        // propierties o donde sea que lo vaya a guardar
        JPanel panelBorderL = new JPanel(new BorderLayout(0,0));

        // Panel Grid rellenar informacion
        JPanel panel1 = new JPanel();
        panel1.setLayout(new GridLayout(3,2));
        JLabel etiquetaUser = new JLabel("Usuario:");
        tfUser = new JTextField();
        JLabel etiquetaPass = new JLabel("Password:");
        tfPass = new JTextField();
        panel1.add(etiquetaUser);
        panel1.add(tfUser);
        panel1.add(etiquetaPass);
        panel1.add(tfPass);

        // Panel boton go
        JPanel panelBoton = new JPanel();
        btnGo = new JButton("Aceptar");
        btnGo.setActionCommand("botonAceptarCambioDatosConexion");
        panelBoton.add(btnGo);

        // Meto los datos en el BorderLayout panel y luego meto el panel con all al JDialog
        panelBorderL.add(panel1, BorderLayout.CENTER);
        panelBorderL.add(panelBoton,BorderLayout.SOUTH);
        dialogDatosConexion.add(panelBorderL);


        dialogDatosConexion.setLocationRelativeTo(null);
        dialogDatosConexion.pack();
        dialogDatosConexion.setVisible(true);
        return dialogDatosConexion;
    }

    /**
     * Se muestra un JOptionPane con los datos de conexion.
     */
    public void leerProperties(){
        Properties configuracion = new Properties();
        String user,password;
        try {
            configuracion.load(new FileInputStream("configuracion.conf"));
            user = configuracion.getProperty("user");
            password = configuracion.getProperty("password");
            JOptionPane.showMessageDialog(null,"Informacion de datos de conexion. Usuario: " + user + ". Password: " + password,
                    "Informacion",JOptionPane.INFORMATION_MESSAGE);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
