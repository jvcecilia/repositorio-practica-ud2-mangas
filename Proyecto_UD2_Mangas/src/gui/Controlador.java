package gui;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by DAM on 18/12/2018.
 */
public class Controlador implements ActionListener{

    private Vista vista;
    private Modelo modelo;

    public Controlador(Vista vista, Modelo modelo){
        this.vista = vista;
        this.modelo = modelo;
        if(!this.modelo.fichero.exists()) {
            System.out.println("Existe");
            this.modelo.generarProperties("root", "");
        }
        this.modelo.conectarALaBBDD(); // Conecto a la base de datos con los datos que estan en el fichero properties
        this.vista.datosConexion.addActionListener(this);
        this.vista.mirarDatosConexion.addActionListener(this);
    }


    @Override
    public void actionPerformed(ActionEvent e)
    {
        String comando = e.getActionCommand();
        switch (comando){
            case "leerDatosConexion":
                System.out.println("sale");
                this.vista.leerProperties();
                break;
            case "escribirDatosConexion":
                this.vista.dialogDatosConexion();
                this.vista.btnGo.addActionListener(this);
                break;
            case "botonAceptarCambioDatosConexion":
                System.out.println("ey");
                this.modelo.generarProperties(vista.tfUser.getText(),vista.tfPass.getText());
                System.out.println(vista.tfUser.getText() + vista.tfPass.getText());
                this.vista.dialogDatosConexion().setVisible(false);
                JOptionPane.showMessageDialog(null,"Se cierra el programa para que se cambien los datos de conexion","Actualizar datos",JOptionPane.INFORMATION_MESSAGE);
                System.exit(0);
                break;
        }
    }
}
