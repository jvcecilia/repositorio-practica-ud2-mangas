package gui;

import javax.swing.*;
import java.io.*;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

/**
 * Created by DAM on 18/12/2018.
 */
public class Modelo {
    private java.sql.Connection conexion;
    private Properties configuracion;
    File fichero = new File("configuracion.conf");
    String user;
    String password;

    public Modelo() {

    }


    //TODO lo que quiero es generar un properties distinto si el admin decide cambiar el user y el pw
    public void generarProperties(String usuario, String pw){
        configuracion = new Properties();
        configuracion.setProperty("user", usuario);
        configuracion.setProperty("password", pw);
        try {
            configuracion.store(new FileOutputStream(fichero),
                    "Fichero que almacena los datos de conexion.");
        } catch (FileNotFoundException fnfe ) {
            fnfe.printStackTrace();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    public void conectarALaBBDD(){
        try{
            configuracion = new Properties();
            try {
                configuracion.load(new FileInputStream("configuracion.conf"));
                user = configuracion.getProperty("user");
                password = configuracion.getProperty("password");
                conexion = java.sql.DriverManager.getConnection("jdbc:mysql://localhost:3306/mangas",user,password);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }catch(SQLException e){
            JOptionPane.showMessageDialog(null, e.getMessage()); // Muestra error si hay problemas en la conexion.
        }
    }

    public void pedirDatosAccesoBBDD(){


    }

}
