package main;

import gui.Controlador;
import gui.Modelo;
import gui.Vista;

/**
 * Created by DAM on 18/12/2018.
 */
public class Main {

    public static void main(String[] args) {

    Modelo modelo = new Modelo();
    Vista vista = new Vista();
    Controlador controlador = new Controlador(vista,modelo);
    }

}
