CREATE DATABASE if not exists mangas;
USE mangas;

CREATE TABLE if not exists tienda(
id int auto_increment primary key,
nombre VARCHAR(20) not null UNIQUE,
descripcion VARCHAR(40) not null,
direccion VARCHAR(50) not null,
encargado VARCHAR(20) not null,
no_trabajadores int
);
